MARKER=/marker.txt
RS_MEMBER="{ \"_id\": 0, \"host\": \"127.0.0.1:27017\", \"priority\": 2 }"

if [ ! -f "$MARKER" ]; then
    echo "Initializing replica set..."
    sleep 15
    mongo --eval "rs.initiate({ \"_id\": \"rs0\", \"members\": [${RS_MEMBER}] });"
    touch ${MARKER}
    mkdir -p /data
    echo "Replica set initialized."
fi
