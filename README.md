# MongoDB 5+ Builder for Raspberry Pi 4 64-bit

[![Docker Pulls](https://img.shields.io/docker/pulls/realivanjx/mongodb-rpi.svg)](https://hub.docker.com/r/realivanjx/mongodb-rpi/)


### Build instruction
* Build the image builder with `docker build -f mongoruntime.Dockerfile -t mongodb-rpi .`
